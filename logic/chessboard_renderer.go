package logic

// Task : What is inside this structure?
/*
   Answer : In this structure is the three elements of type uint
*/
type ChessBoardRenderer struct {
	square_size_in_pixel        uint
	number_of_squares_horizonal uint
	number_of_squares_vertical  uint
}

// Task : Explain what this function do? What arguments this function request from the caller? What this function returns?
/*
   Answer : This function makes the new exemplar of ChessBoardRenderer with pointer to him, this function requests square_size, horizonal_squares and vertical_squares of type uint,
            this function returns a new exemplar of ChessBoardRenderer with pointer to him
*/
func NewChessBoardRenderer(square_size uint, horizonal_squares uint, vertical_squares uint) (this *ChessBoardRenderer) {
	this = new(ChessBoardRenderer)
	this.square_size_in_pixel = square_size
	this.number_of_squares_horizonal = horizonal_squares
	this.number_of_squares_vertical = vertical_squares
	return
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method draws(render) black and white squares alternately counting in parallel the amount of squares,
            and draws this with using another method (DrawSquare), this method requests the canvas_drawer of type(struct) CanvasDrawer
*/
func (this *ChessBoardRenderer) renderSquares(canvas_drawer *CanvasDrawer) {
	white_color := Color{Red: 255, Green: 255, Blue: 255}
	black_color := Color{Red: 0, Green: 0, Blue: 0}

	square_counter := uint(0)

	for square_x := range this.number_of_squares_horizonal {
		for square_y := range this.number_of_squares_vertical {

			current_square_start_x := square_x * this.square_size_in_pixel
			current_square_start_y := square_y * this.square_size_in_pixel
			//									↑
			//			These 2 lines says thats from where we are starting to draw our square.
			//			P.S. This is the notice for me   :)

			current_square_size := this.square_size_in_pixel
			var current_square_color Color

			if (square_counter % 2) == 0 {
				current_square_color = white_color
			} else {
				current_square_color = black_color
			}

			canvas_drawer.DrawSquare(
				current_square_start_x,
				current_square_start_y,
				current_square_size,
				current_square_color)

			square_counter++
		}
	}
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method draws the red borders for all our squares with using the another methods (DrawHorisontalLine, DrawVerticalLine, GetCanvasHeight, GetCanvasWidth),
            this method requests the canvas_drawer of type(struct) CanvasDrawer
*/
func (this *ChessBoardRenderer) renderSquareBorders(canvas_drawer *CanvasDrawer) {
	red_color := Color{Red: 255, Green: 0, Blue: 0}

	for horizontal_line_index := range this.number_of_squares_vertical {
		horizonal_line_y := horizontal_line_index * this.square_size_in_pixel
		canvas_drawer.DrawHorizontalLine(horizonal_line_y, red_color)
	}
	canvas_drawer.DrawHorizontalLine(canvas_drawer.GetCanvasHeight()-1, red_color)

	for vertical_line_index := range this.number_of_squares_horizonal {
		vertical_line_x := vertical_line_index * this.square_size_in_pixel
		canvas_drawer.DrawVerticalLine(vertical_line_x, red_color)
	}
	canvas_drawer.DrawVerticalLine(canvas_drawer.GetCanvasWidth()-1, red_color)
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method makes the final render of chess board with using another methods namely: NewCanvas, NewCanvasDrawer, renderSquares, renderSquaresBorders.
            This method requests the canvas of type(struct) Canvas
*/
func (this *ChessBoardRenderer) Render() (canvas *Canvas) {
	canvas_width := this.number_of_squares_horizonal * this.square_size_in_pixel
	canvas_height := this.number_of_squares_vertical * this.square_size_in_pixel

	canvas = NewCanvas(canvas_width, canvas_height)
	canvas_drawer := NewCanvasDrawer(canvas)

	this.renderSquares(canvas_drawer)
	this.renderSquareBorders(canvas_drawer)

	return canvas
}
