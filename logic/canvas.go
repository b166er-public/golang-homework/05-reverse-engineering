package logic

// Task : What is inside this structure?
/*
   Answer : Inside this structure is the uint elements width and height, and an array with custom type Color,
            in these array are all color information to all pixels on canvas
*/
type Canvas struct {
	width  uint
	height uint
	pixels []Color
}

// Task : Explain what this function do? What arguments this function request from the caller? What this function returns?
/*
   Answer : This function makes the new exemplar of canvas(field) and with pointer to him,
            this function requests the width and height of type uint from structure Canvas,
            this function returns the new exemplar of Canvas with pointer to this structure
*/
func NewCanvas(width uint, height uint) (this *Canvas) {
	this = new(Canvas)
	this.width = width
	this.height = height
	this.pixels = make([]Color, width*height)
	return
}

// Task : Explain what this method do? What arguments this method request from the caller? What means the return values?
/*
   Answer : This method gets the index of current pixel, but if these pixel is outside the canvas,
            we are giving him the false flag and index 0. This method requests the x and y (uint) coordinates.
            With the return values we are flaging the pixels that are in canvas, and those that are outside canvas
*/
func (this *Canvas) getPixelIndex(x uint, y uint) (index uint, valid bool) {
	if (x < this.width) && (y < this.height) {
		index = this.width*y + x
		valid = true
	} else {
		index = 0
		valid = false
	}
	return
}

// Task : Explain what this method do? What means the return value?
/*
   Answer : This method gets the width of canvas(field), and returns the number of type uint
*/
func (this *Canvas) GetWidth() uint {
	return this.width
}

// Task : Explain what this method do? What means the return value?
/*
   Answer : This method gets the height of canvas (field), and returns the number of type uint
*/
func (this *Canvas) GetHeight() uint {
	return this.height
}

// Task : Explain what this method do? What arguments this method request from the caller? What means the return value?
/*
   Answer : This method gets the current pixel color with the checking if this pixel located on field(canvas),
            this method requests the coordinates x and y of type uint, this function returns the color of current pixel,
            but!!! if this current pixel located outside of field(canvas) he returns the default color
*/
func (this *Canvas) getPixelColor(x uint, y uint) Color {
	pixel_index, index_valid := this.getPixelIndex(x, y)
	if index_valid {
		return this.pixels[pixel_index]
	} else {
		return Color{Red: 0, Green: 0, Blue: 0}
	}
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method sets the color of the current pixel with the selected color with type Color,
            but if this pixel is outside of field(canvas) he sets nothing, this method requests the coordinates of current pixel x and y of type uint,
            and this method requests the color of custom type Color
*/
func (this *Canvas) setPixelColor(x uint, y uint, color Color) {
	pixel_index, index_valid := this.getPixelIndex(x, y)
	if index_valid {
		this.pixels[pixel_index] = color
	}
}
