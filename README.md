# 05-reverse-engineering

The task for this homework is inverse. The code is already written, but you have to analyze what the code is doing and add comments to the code that explain your thoughts.

## Main tasks
- Read the code very carefully and be prepared to give answers to every single line within the code. 
- There, where you find comments with the word `Task` you have to write down your answer in the following `Answer : ???` comment
- If you see uknown methods or functions, try to find out what they do
- If you see packages which are imported, try to read the documentation of this package online
