package logic

// Task : What is inside this structure?
/*
   Answer : Tree structure elements of type uint8
*/
type Color struct {
	Red   uint8
	Green uint8
	Blue  uint8
}
