package logic

// Task : What is inside this structure?
/*
   Answer : Inside this function is Pointer to another struct named Canvas
*/
type CanvasDrawer struct {
	canvas *Canvas
}

// Task : Explain what this function do? What arguments this function request from the caller? What this function returns?
/*
   Answer : This function makes new drawer for canvas. This function get structure element (*CanvasDrawer)
*/
func NewCanvasDrawer(canvas *Canvas) (this *CanvasDrawer) {
	this = new(CanvasDrawer)
	this.canvas = canvas
	return
}

// Task : Explain what this method do? What this method returns?
/*
   Answer : This mathod gets width from canvas(field), this method returns the uint number of width from canvas
*/
func (this *CanvasDrawer) GetCanvasWidth() uint {
	return this.canvas.width
}

// Task : Explain what this method do? What this method returns?
/*
   Answer : This method gets height from canvas(field), this method returns the uint number of height from canvas
*/
func (this *CanvasDrawer) GetCanvasHeight() uint {
	return this.canvas.height
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method makes the horizontal line with the given color, and this method request the y(uint) and color(Color) arguments
*/
func (this *CanvasDrawer) DrawHorizontalLine(y uint, color Color) {
	for x := range this.canvas.width {
		this.canvas.setPixelColor(x, y, color)
	}
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method makes the vertical line with the given color, and this method request the x (uint) and color (Color) arguments
*/
func (this *CanvasDrawer) DrawVerticalLine(x uint, color Color) {
	for y := range this.canvas.height {
		this.canvas.setPixelColor(x, y, color)
	}
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method draws the rectangle, with 2 cicles,
            and this method request the start_x (uint), start_y (uint), x_size (uint), y_size (uint), color (Color) arguments
*/
func (this *CanvasDrawer) DrawRectangle(start_x uint, start_y uint, x_size uint, y_size uint, color Color) {
	for x := range x_size {
		for y := range y_size {
			this.canvas.setPixelColor(start_x+x, start_y+y, color)
		}
	}
}

// Task : Explain what this method do? What arguments this method request from the caller?
/*
   Answer : This method draws the Square, with using DrawRectangle method, this method requires the same arguments as DrawRectangle method start_x (uint), start_y (uint), size (uint), color (Color)
*/
func (this *CanvasDrawer) DrawSquare(start_x uint, start_y uint, size uint, color Color) {
	this.DrawRectangle(start_x, start_y, size, size, color)
}
