package main

import (
	"log"
	"os"
	"private/reverse_engineering/logic"
	"strconv"
)

func main() {

	// Task : Explain what this function do? What is the result of the program?
	/*
	   Answer : This is the main function that uses another functions, methods and packages of standard library to make a chess board
	            and then export these board into an another format(png) and show the path to this image.
	*/

	error_parsing_command_line_arguments := false //This line means that we doesn't have any problems in parsing of arguments from command line

	//Setting the variables by arguments from command line with using os.Args
	square_size_in_pixel_string := os.Args[1]
	number_of_squares_horizonal_string := os.Args[2]
	number_of_squares_vertical_string := os.Args[3]
	export_path := os.Args[4]

	//We are converting the 1 argument and checking if these argument have a problem
	square_size_in_pixel, parse_error := strconv.Atoi(square_size_in_pixel_string)
	if parse_error != nil {
		error_parsing_command_line_arguments = true
	}

	//We are converting the 2 argument and checking if these argument have a problem
	number_of_squares_horizonal, parse_error := strconv.Atoi(number_of_squares_horizonal_string)
	if parse_error != nil {
		error_parsing_command_line_arguments = true
	}

	//We are converting the 3 argument and checking if these argument have a problem
	number_of_squares_vertical, parse_error := strconv.Atoi(number_of_squares_vertical_string)
	if parse_error != nil {
		error_parsing_command_line_arguments = true
	}

	//Here we are checking the error_parsing_command_line_arguments, if it is true we are printing an error message and write text asking to check the arguments.
	//But if error_parsing_command_line_arguments is false, then we are using the another methods to draw the chess board, in the end of these function
	//we are writing to the console, where is the image of our chess board.
	if !error_parsing_command_line_arguments {
		chess_board_renderer := logic.NewChessBoardRenderer(
			uint(square_size_in_pixel),
			uint(number_of_squares_horizonal),
			uint(number_of_squares_vertical))

		chess_board_canvas := chess_board_renderer.Render()

		canvas_writer := logic.NewCanvasWriter(chess_board_canvas, export_path)
		canvas_writer.Write()
		log.Println("Chessboard image exported : ", export_path)
	} else {
		log.Fatal("Please provide the right arguments!")
	}

}
